import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-anagram',
  templateUrl: './anagram.component.html',
  styleUrls: ['./anagram.component.scss']
})
export class AnagramComponent implements OnInit {

  constructor() { }
  result = '';

  ngOnInit(): void {
  }

  compare(a:string,b:string) {
    if(a =='' && b==''){
      this.result = '';
    }else{
      var c = a.replace(/\W+/g, '').toLowerCase().split("").sort().join("");
      var d = b.replace(/\W+/g, '').toLowerCase().split("").sort().join("");
      this.result= (c ===d) ? "Anagram":"Not anagram";

    }
    
    
  }

}
