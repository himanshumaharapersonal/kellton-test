import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationComponent } from './registration.component';

describe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('validation with all blank fields',()=>{
    component.registrationForm.patchValue({
      'first_name':'',
      'last_name':'',
      'phone':'',
      'country':''
    });

    const firstname = component.registrationForm.controls.first_name;
    const lastname = component.registrationForm.controls.last_name;
    const phone = component.registrationForm.controls.phone;
    const country = component.registrationForm.controls.country;
    
    expect(firstname.hasError('required')).toBeTruthy();
    expect(lastname.hasError('required')).toBeTruthy();
    expect(phone.hasError('required')).toBeTruthy();
    expect(country.hasError('required')).toBeTruthy();
  });

  it('validation with First name and Last name are less than 5 characters',()=>{
    component.registrationForm.patchValue({
      'first_name':'Him',
      'last_name':'Mah',
      'phone':'',
      'country':''
    });

    const firstname = component.registrationForm.controls.first_name;
    const lastname = component.registrationForm.controls.last_name;
    
    expect(firstname.hasError('minlength')).toBeTruthy();
    expect(lastname.hasError('minlength')).toBeTruthy();
    
  });

  it('validation with Phone numberic',()=>{
    component.registrationForm.patchValue({
      'first_name':'',
      'last_name':'',
      'phone':'ljflsdjf',
      'country':''
    });

    const firstname = component.registrationForm.controls.first_name;
    const phone = component.registrationForm.controls.phone;
    
    expect(phone.hasError('pattern')).toBeTruthy();
    
  });

  it('validation with all valid Fields',()=>{
    component.registrationForm.patchValue({
      'first_name':'Himanshu',
      'last_name':'Mahara',
      'phone':'9717735701',
      'country':'India'
    });

    const firstname = component.registrationForm.controls.first_name;
    const lastname = component.registrationForm.controls.last_name;
    const phone = component.registrationForm.controls.phone;
    const country = component.registrationForm.controls.country;
    
    expect(component.registrationForm.valid).toBeTruthy();
  });
});
