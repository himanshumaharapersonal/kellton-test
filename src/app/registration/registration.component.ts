import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  registrationForm = new FormGroup({
    'first_name': new FormControl('',[Validators.required,Validators.minLength(5)]),
    'last_name': new FormControl('',[Validators.required,Validators.minLength(5)]),
    'phone': new FormControl('',[Validators.required,Validators.pattern('^[0-9]*$')]),
    'country': new FormControl('',[Validators.required]),
  });
  isSubmitted = false;
  message = '';

  constructor() { }

  ngOnInit(): void {
  }

  formControls(field:any){
    return this.registrationForm.controls[field];
  }

  register(){
    this.isSubmitted = true;
   
    if(this.registrationForm.valid){
      this.message = 'Your account has been created.';
      this.registrationForm.reset();
      this.isSubmitted = false;
     
    }
    console.log(document.querySelector(".first_name_error"));
  }

}
