import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { AnagramComponent } from './anagram/anagram.component';
import { TempTrackerComponent } from './temp-tracker/temp-tracker.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    AnagramComponent,
    TempTrackerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
