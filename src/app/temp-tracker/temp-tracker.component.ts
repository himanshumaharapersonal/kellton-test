import { Component, OnInit, ElementRef, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-temp-tracker',
  templateUrl: './temp-tracker.component.html',
  styleUrls: ['./temp-tracker.component.scss']
})
export class TempTrackerComponent implements OnInit {

  constructor() { }
  result:number|any;
  temperature: Array<Number> = [27,25,24];
  ngOnInit(): void {
  }

  get max(){
    const temp:any = this.temperature;
    return Math.max(...temp);
  }
  get min(){
    const temp:any = this.temperature;
    return Math.min(...temp);
  }

  get mode() {
		let table:any = {};
		this.temperature.forEach((value:any) => (table[value] = table[value] + 1 || 1));

		let modes: Array<Number>= [];
		let max = 0;
		for (const key in table) {
			const value = parseFloat(key);
			const count = table[key];
			if (count > max) {
				modes = [value];
				max = count;
			} else if (count === max) modes.push(value);
    }
    
    if (modes.length === Object.keys(table).length) modes = [];

		return modes;
  }

  get mean() {
		return this.temperature.reduce((sum:number, value:Number) => sum + +value, 0) / this.temperature.length;
	}
  
  


  insert(temp:any){
    this.temperature.push(temp.value);
    temp.value = '';

  }

  numberOnlyWithMax(event:any,input:any,max:number): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }

    if(input.value > max){
      input.value = max;
      return false;
    }

    if(input.value < 0){
      input.value = '';
      return false;
    }
    
    return true;

  }



}
